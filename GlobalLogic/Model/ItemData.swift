//
//  ItemData.swift
//  GlobalLogic
//
//  Created by Juan Carlos Cardozo on 28/06/2021.
//

import Foundation

struct Item: Decodable, Identifiable {
    let id = UUID()
    let title: String
    let description: String
    let image: String
}

class Api: Codable, Identifiable{
    
    let ItemsURL = "https://private-f0eea-mobilegllatam.apiary-mock.com/list"
    
    func getItems(completion: @escaping([Item]) -> ()){
        let urlString = ItemsURL
  
        guard let url = URL(string: urlString) else { return }
    
        let session = URLSession(configuration: .default)
            
        session.dataTask(with: url){ (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
                
            guard let data = data else {
                print("Empty Response")
                return
            }
            
            let decoder = JSONDecoder()
            do{
                let decodeData = try decoder.decode([Item].self, from: data)
                print(decodeData)
                
                DispatchQueue.main.async {
                    completion(decodeData)
                }
            }catch{
                print(error)
            }
        }
        .resume()
    }
}

