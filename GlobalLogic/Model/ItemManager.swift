//
//  ItemManager.swift
//  GlobalLogic
//
//  Created by Juan Carlos Cardozo on 28/06/2021.
//

import Foundation

struct ItemManager{
    
    let ItemsURL = "https://private-f0eea-mobilegllatam.apiary-mock.com/list"
    
    func getItems(completion: @escaping([])){
        let urlString = ItemsURL
  
        guard let url = URL(string: urlString) else { return }
    
        let session = URLSession(configuration: .default)
            
        session.dataTask(with: url){ (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
                
            guard let data = data else {
                print("Empty Response")
                return
            }
                
            self.parseJSON(items: data)
        }
        .resume()
    }
    
    func parseJSON(items: Data){
        let decoder = JSONDecoder()
        
        do{
            let decodeData = try decoder.decode(ItemData.self, from: items)
            print(decodeData.items)
        }catch{
            print(error)
        }

    }
}
