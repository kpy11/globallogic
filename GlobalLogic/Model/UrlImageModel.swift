//
//  SwiftUIView.swift
//  GlobalLogic
//
//  Created by Juan Carlos Cardozo on 28/06/2021.
//

import SwiftUI

class UrlImageModel: ObservableObject {
    @Published var image: UIImage?
    var urlString: String?
    var imgCache = ImgCache.getImgCache()
    
    init(urlString: String?){
        self.urlString = urlString
        loadImg()
    }
    
    func loadImg(){
        if loadImgFromCache() {
            return
        }
        loadImgFromURL()
    }
    
    func loadImgFromCache() -> Bool {
        guard let urlString = urlString else {
            return false
        }
        
        guard let cacheImg = imgCache.get(forKey: urlString) else {
            return false
        }
        
        image = cacheImg
        return true
    }
    
    func loadImgFromURL(){
        
        guard let urlString = urlString else {
            return
        }
        
        let url = URL(string: urlString)!
        let task = URLSession.shared.dataTask(with: url, completionHandler:
            getImgResponse(data:response:error:))
        
        task.resume()
    }
    
    func getImgResponse(data: Data?, response: URLResponse?, error: Error?){
        
        guard error == nil else {
            print("Error: \(error)")
            return
        }
        
        guard let data = data else {
            print("No data found...")
            return
        }
        
        DispatchQueue.main.async {
            guard let img = UIImage(data: data) else {
                return
            }
            
            self.imgCache.set(forKey: self.urlString!, image: img)
            self.image = img
        }
    }
}

class ImgCache {
    var cache = NSCache<NSString, UIImage>()
    
    func get(forKey: String) -> UIImage? {
        return cache.object(forKey: NSString(string: forKey))
    }
    
    func set(forKey: String, image: UIImage){
        cache.setObject(image, forKey: NSString(string: forKey))
    }
}

extension ImgCache{
    private static var imgCache = ImgCache()
    static func getImgCache() -> ImgCache {
        return imgCache
    }
}
