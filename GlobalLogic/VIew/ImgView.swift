//
//  ImgView.swift
//  GlobalLogic
//
//  Created by Juan Carlos Cardozo on 28/06/2021.
//

import SwiftUI

struct ImgViewMini: View {
    @ObservedObject var urlImageModel: UrlImageModel
    
    init(urlString: String?){
        urlImageModel = UrlImageModel(urlString: urlString)
    }
    
    var body: some View {
        Image(uiImage: urlImageModel.image ?? ImgView.defaultImg!)
            .resizable()
            .scaledToFill()
            .frame(width: 70, height: 70, alignment: .center)
            .cornerRadius(4.0)
            .padding(.vertical, 4)
    }
    
    static var defaultImg = UIImage(named: "laptop")
}

struct ImgView: View {
    @ObservedObject var urlImageModel: UrlImageModel
    
    init(urlString: String?){
        urlImageModel = UrlImageModel(urlString: urlString)
    }
    
    var body: some View {
        Image(uiImage: urlImageModel.image ?? ImgView.defaultImg!)
            .resizable()
            .scaledToFit()
            .cornerRadius(10.0)
            .frame(height: 150)
            .padding(.vertical, 4)
    }
    
    static var defaultImg = UIImage(named: "laptop")
}


struct ImgView_Previews: PreviewProvider {
    static var previews: some View {
        ImgView(urlString: nil)
    }
}
