//
//  ItemDetailView.swift
//  GlobalLogic
//
//  Created by Juan Carlos Cardozo on 28/06/2021.
//

import SwiftUI

struct ItemDetailView: View {
    
    var item: Item
    
    var body: some View {
        VStack(spacing: 10){
            ImgView(urlString: item.image)
                
            Text(item.title)
                .font(.title2)
                .fontWeight(.semibold)
                .lineLimit(2)
                .multilineTextAlignment(.center)
                .padding(.horizontal)
                
            Text(item.description)
                .font(.body)
                .multilineTextAlignment(.center)
                .padding()
        }
    }
}

struct ItemDetailView_Previews: PreviewProvider {
    var item:Item
    static var previews: some View {
        ItemDetailView(item: Item(title: "", description: "", image: ""))
    }
}
