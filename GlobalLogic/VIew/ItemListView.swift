//
//  ContentView.swift
//  GlobalLogic
//
//  Created by Juan Carlos Cardozo on 28/06/2021.
//

import SwiftUI

struct ItemListView: View {
    
    @State var items: [Item] = []
     
    var body: some View {
        NavigationView{
            List(items, id:\.id){ item in
                
                NavigationLink(
                    destination: ItemDetailView(item: item), label: {
                        ItemCell(item: item)
                    }
                )
            }
            .onAppear{
                Api().getItems { items in
                    self.items = items
                }
            }
            .navigationTitle("Global Logic")
        }
    }
}

struct ItemCell: View {
    var item: Item
    
    var body: some View {
        HStack{
            ImgViewMini(urlString: item.image)
            
            VStack(alignment: .leading, spacing: 5) {
                Text(item.title)
                    .fontWeight(.semibold)
                    .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
                Text(item.description)
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                    .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
                    .minimumScaleFactor(0.8)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ItemListView()
    }
}
